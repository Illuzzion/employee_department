from rest_framework.routers import DefaultRouter

from api.v1.viewsets import EmployeeViewSet
from api.v1.viewsets.department import DepartmentViewSet
from api.v1.viewsets.project import ProjectViewSet

api_router = DefaultRouter()
api_router.register('employee', EmployeeViewSet, basename='employee')
api_router.register('department', DepartmentViewSet, basename='department')
api_router.register('project', ProjectViewSet, basename='project')

urlpatterns = api_router.urls
