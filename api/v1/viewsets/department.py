from django.db.models import Count, Sum
from rest_framework import viewsets
from rest_framework.permissions import AllowAny

from ..serializers.department import DepartmentSerializer
from department.models import Department


class DepartmentViewSet(viewsets.ModelViewSet):
    queryset = Department.objects.all()
    serializer_class = DepartmentSerializer
    permission_classes = [AllowAny]

    def get_queryset(self):
        qs = super().get_queryset()
        return qs.prefetch_related(
            'employees'
        ).annotate(
            employees_count=Count('employees__id'),
            salary_sum=Sum('employees__salary')
        )
