from django.http import Http404
from django_filters import rest_framework as filters
from rest_framework import viewsets, status
from rest_framework.decorators import action
from rest_framework.pagination import PageNumberPagination
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response

from api.v1.serializers import EmployeeSerializer
from employee.models import Employee


class EmployeeViewSet(viewsets.ModelViewSet):
    queryset = Employee.objects.select_related().all()
    serializer_class = EmployeeSerializer
    filter_backends = (filters.DjangoFilterBackend,)
    filterset_fields = ('last_name', 'department')
    pagination_class = PageNumberPagination
    permission_classes = [IsAuthenticated]

    @action(methods=['GET'], detail=False, url_path='department/(?P<department_id>[^/.]+)', url_name='by_department')
    def by_department(self, request, department_id,  *args, **kwargs):
        qs = super().get_queryset().filter(department=department_id)
        if not qs:
            raise Http404

        res = self.get_serializer(data=qs, many=True)
        res.is_valid()
        return Response(res.data, status=status.HTTP_200_OK)
