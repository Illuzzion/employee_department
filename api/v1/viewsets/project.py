from rest_framework import viewsets

from api.v1.serializers.project import ShortProjectSerializer, ExtProjectSerializer
from project.models import Project


class ProjectViewSet(viewsets.ModelViewSet):
    queryset = Project.objects.all()

    def get_serializer_class(self):
        actions = {
            'list': ShortProjectSerializer,
            'retrieve': ExtProjectSerializer
        }

        return actions.get(self.action, ShortProjectSerializer)
