from rest_framework import serializers

from api.v1.serializers import EmployeeSerializer
from project.models import Project


class ShortProjectSerializer(serializers.ModelSerializer):
    class Meta:
        model = Project
        fields = 'id', 'name'


class ExtProjectSerializer(serializers.ModelSerializer):
    manager = EmployeeSerializer()
    project_employees = EmployeeSerializer(many=True)

    class Meta:
        model = Project
        fields = 'id', 'name', 'manager', 'project_employees'
