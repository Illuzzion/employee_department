from rest_framework import serializers

from department.models import Department


class DepartmentSerializer(serializers.ModelSerializer):
    employees_count = serializers.IntegerField()
    salary_sum = serializers.IntegerField()

    class Meta:
        model = Department
        fields = (
            'id',
            'name',
            'director',
            'employees_count',
            'salary_sum',
        )
