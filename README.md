# Тестовое задание

Задание выполнено с использованием Django+DRF+PostgreSQL.

Для установки необходимых библиотек используется `poetry`.

В качестве БД используется `PostgreSQL` в Docker контейнере.

Чтобы запустить задание, нужно перейти в директорию проектов, выполнить следующие команды:

```bash
# колнируем репозиторий
git clone https://gitlab.com/Illuzzion/employee_department.git
cd employee_department/
# создадим .env переменные, для тестового задания
cp .env.example .env
cp docker/db/.env.example docker/db/.env
# установим необходимое и перейдем в venv
poetry install
poetry shell
# БД
docker-compose up -d
# создаем пользователя Django
./manage.py createsuperuser
# Запустим встроенный сервер
./manage.py runserver
```

## Пояснения

по адресу: 
- `http://127.0.0.1:8000/admin/` доступна админка
- `http://127.0.0.1:8000/api/v1/` реализованные endpoints
- `http://127.0.0.1:8000/docs/` Swagger


> API для получения списка сотрудников + реализовать фильтр для поиска по фамилии и по id департамента

Реализовано с помощью библиотеки `django-filter` и дополнительно реализован метод фильтрации сотрудников с помощью [`этого action`](https://gitlab.com/Illuzzion/employee_department/-/blob/master/api/v1/viewsets/employee.py#L21)
