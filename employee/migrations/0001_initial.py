# Generated by Django 4.2.2 on 2023-07-01 16:16

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('department', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Employee',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('first_name', models.CharField(max_length=100, verbose_name='Имя')),
                ('last_name', models.CharField(db_index=True, max_length=100, verbose_name='Фамилия')),
                ('surname', models.CharField(max_length=100, verbose_name='Отчество')),
                ('photo', models.ImageField(blank=True, upload_to='upload/users', verbose_name='Фото')),
                ('job_name', models.CharField(blank=True, max_length=100, verbose_name='Должность')),
                ('salary', models.DecimalField(decimal_places=2, default=0, max_digits=10, verbose_name='Оклад')),
                ('birthday', models.DateField(blank=True, null=True, verbose_name='День рождения')),
                ('department', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='employees', to='department.department', verbose_name='Департамент')),
            ],
            options={
                'verbose_name': 'Сотрудник',
                'verbose_name_plural': 'Сотрудники',
                'db_table': 'employees',
                'ordering': ('last_name',),
                'unique_together': {('id', 'department')},
            },
        ),
    ]
