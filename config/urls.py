from django.contrib import admin
from django.urls import path, include
from drf_yasg import openapi
from drf_yasg.views import get_schema_view
from rest_framework import permissions

from api import urls as api_urls

schema_view = get_schema_view(
    openapi.Info(
        title="API",
        default_version='v1',
        description="Тестовое задание",
        contact=openapi.Contact(email="79fz270704@gmail.com"),
    ),
    public=True,
    permission_classes=[permissions.IsAuthenticated],
)

urlpatterns = [
    path('admin/', admin.site.urls),
    path('api/', include(api_urls)),
    path('docs/', schema_view.with_ui('swagger', cache_timeout=0), name='schema-swagger-ui'),
]
