from django.db import models


class Project(models.Model):
    name = models.CharField('Название проекта', blank=True)
    manager = models.ForeignKey('employee.Employee', on_delete=models.SET_NULL, related_name='managers', blank=True,
                                null=True)
    project_employees = models.ManyToManyField('employee.Employee')

    def __str__(self):
        return self.name